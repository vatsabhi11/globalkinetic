Feature:  Automate Weather Map API calls

  @success
  Scenario Outline: To Register a weather station
    Given User is registering a weather station with "<External_ID>", "<WeatherStation>", <latitude>, <longitude>, <altitude>
    Then user should get a statuscode as 201
    And User should get a content type as "application/json; charset=utf-8"
    Then User is validating the weather station register response schema
    Examples:
      | External_ID        | WeatherStation        | latitude | longitude | altitude |
      | WeatherStation_123 | CapeTown Test Station | 37.78    | -121.90   | 120      |

  @success
  Scenario: To validate the newly register weather station exists in the Weather API
    Given that newly register weather station should be available in the Weather Map API
    Then user should get a statuscode as 200
    And User should get a content type as "application/json; charset=utf-8"
    Then User is validating the weather station info response schema

  @success
  Scenario Outline:  User is to update the name of the weather station
    Given that user should be able to update the weather station with "<External_ID>", "<WeatherStation>", <latitude>, <longitude>, <altitude>
    Then user should get a statuscode as 200
    And User should get a content type as "application/json; charset=utf-8"
    Then User is validating the weather station info response schema

    Examples:
      | External_ID        | WeatherStation           | latitude | longitude | altitude |
      | WeatherStation_007 | WesternCape Test Station | 35.65    | -100.65   | 94       |

  @success
  Scenario: User is deleting the newly register weather station and to validate it is deleted
    Given that user should be able to delete the newly register weather station
    Then user should get a statuscode as 204
    When user trying to search the deleted weather station
    Then user should get a statuscode as 404
    And  "Station not found" message in the response

  @error
  Scenario: user is trying to delete a station which does not exist
    Given that user trying to delete a non exist station
    Then user should get a statuscode as 404
    And  "Station not found" message in the response

  @error
  Scenario Outline: user is trying to register a weather station with empty parameters
    Given User is registering a weather station with "<External_ID>", "<WeatherStation>", <latitude>, <longitude>, <altitude>
    Then user should get a statuscode as 400
    Examples:
      | External_ID         | WeatherStation        | latitude | longitude | altitude |
      |                     | CapeTown Test Station | 37.78    | -121.90   | 120      |
      | WeatherStation_0001 |                       | 37.78    | -121.90   | 120      |