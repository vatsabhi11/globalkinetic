Feature: Regression pack for SauceDemo

  @success
  Scenario Outline: To validate the application functionality to make online orders
    Given that "<User>" opened the login page and entered the correct credentials
    Then user should able to login to SwagLabs and able to see the product page
    When user is adding the "BackPack", "BikeLight" products into the cart
    Then user should able to see "BackPack", "BikeLight" products in the cart
    And user should be able to checkout after providing the "<FirstName>","<LastName>" and "<PostalCode>" details
    Then user should be navigated to checkout overview page with selected products
    And after finishing the checkout the order successful message should be visible to user
    Then user should logout from SwagLabs

    Examples:
      | User          | FirstName | LastName | PostalCode |
      | standard_user | Abhi      | Vats     | 8001       |

  @error
  Scenario Outline: To validate the application functionality for a locked out user
    Given that "<User>" opened the login page and entered the correct credentials
    Then user should be getting an expected error message defined for a lockedout user
    Examples:
      | User            |
      | locked_out_user |

  @error
  Scenario Outline: To validate the application functionality for a user with incorrect credentials
    Given that "<User>" opened the login page and entered the incorrect credentials
    Then user should be getting an expected error message defined for incorrect credentials
    Examples:
      | User          |
      | standard_user |

  @error
  Scenario Outline: To validate the application functionality to make online orders with problematic user
    Given that "<User>" opened the login page and entered the correct credentials
    Then user should able to login to SwagLabs and able to see the product page
    When user is adding the "BackPack", "BikeLight" products into the cart
    Then user should able to see "BackPack", "BikeLight" products in the cart
    When user trying to checkout after providing the "<FirstName>","<LastName>" and "<PostalCode>" details
    Then user should get a error message on checkout page for a empty last name field
    And user should logout from SwagLabs

    Examples:
      | User         | FirstName | LastName | PostalCode |
      | problem_user | Abhi      | Vats     | 8001       |


