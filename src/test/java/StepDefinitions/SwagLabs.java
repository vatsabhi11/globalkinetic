package StepDefinitions;

import WebdriverWrapper.BaseTest;
import common.Product;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import pageObjects.*;
import utils.Utility;

import java.time.Duration;

public class SwagLabs {

    BaseTest baseTest = new BaseTest();
    WebDriver driver = baseTest.baseDriver();
    Login login = new Login(driver);
    Products products = new Products(driver);
    Cart userInCart = new Cart(driver);
    CheckoutComplete userInCheckoutComplete = new CheckoutComplete(driver);
    CheckOut checkout = new CheckOut(driver);
    ErrorMessage errorMessage = new ErrorMessage(driver);
    MenuItems menuItem = new MenuItems(driver);
    Utility utility = new Utility();
    WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(30));


    @Given("that {string} opened the login page and entered the correct credentials")
    public void navigateToSwagLabsUrl(String user) {

        login.navigateToSwagLabs(driver);
        login.loginToSwagLabs(user, utility.Property("SwagLab_pwd"));
    }

    @Then("user should able to login to SwagLabs and able to see the product page")
    public void userShouldAbleToLoginToSwagLabsAndAbleToSeeTheProductPage() {

        products.waitUntilProductPageLoaded(wait);
        products.toVerifyProductPageTitle();


    }

    @And("user should be able to checkout after providing the {string},{string} and {string} details")
    public void userShouldBeAbleToCheckoutAfterProvidingTheAndDetails(String firstName, String lastName, String postalCode) throws Exception {

        userInCart.isClickonCheckout();
        checkout.enterFirstName(firstName);
        checkout.enterLastName(lastName);
        checkout.enterPostalCode(postalCode);
        checkout.clickOnContinue();

    }

    @Then("user should be navigated to checkout overview page with selected products")
    public void userShouldBeNavigatedToCheckoutOverviewPageWithSelectedProducts() {
        checkout.clickOnFinish();
    }

    @And("after finishing the checkout the order successful message should be visible to user")
    public void afterFinishingTheCheckoutTheOrderSuccessfulMessageShouldBeVisibleToUser() {

        userInCheckoutComplete.toVerifyCheckoutCompleteTitle();
        userInCheckoutComplete.validatingOrderConfirmationMessage();
    }

    @When("user is adding the {string}, {string} products into the cart")
    public void userIsAddingTheProductsIntoTheCart(String product1, String product2) {


        products.userIsAddingProduct(Product.valueOf(product1));
        products.userIsAddingProduct(Product.valueOf(product2));
    }

    @Then("user should able to see {string}, {string} products in the cart")
    public void userShouldAbleToSeeProductsInTheCart(String product1, String product2) {

        products.clickOnCart();
        userInCart.toVerifyCartPageTitle();
        userInCart.toVerifyProductInCart(Product.valueOf(product1));
        userInCart.toVerifyProductInCart(Product.valueOf(product2));


    }

    @Then("user should be getting an expected error message defined for a lockedout user")
    public void userShouldBeGettingAnExpectedErrorMessageDefinedForALockedoutUser() {
        errorMessage.toValidateLockoutUserMessage();
        utility.closeTheSession(driver);
    }

    @Then("user should logout from SwagLabs")
    public void userShouldLogoutFromSwagLabs() throws Exception {
        menuItem.clickOnMenuItem();
        utility.waiting(driver, 2);
        menuItem.clickOnLogOut();
        utility.closeTheSession(driver);
    }

    @Then("user should be getting an expected error message defined for incorrect credentials")
    public void userShouldBeGettingAnExpectedErrorMessageDefinedForIncorrectCredentials() {

        errorMessage.toValidateIncorrectUserNamePasswordMessage();
        utility.closeTheSession(driver);
    }

    @Given("that {string} opened the login page and entered the incorrect credentials")
    public void thatOpenedTheLoginPageAndEnteredTheIncorrectCredentials(String user) {
        login.navigateToSwagLabs(driver);
        login.loginToSwagLabs(user, utility.Property("SwagLab_Incorrect_pwd"));
    }

    @When("user trying to checkout after providing the {string},{string} and {string} details")
    public void userTryingToCheckoutAfterProvidingTheAndDetails(String firstName, String lastName, String postalCode) {
        userInCart.isClickonCheckout();
        checkout.enterFirstName(firstName);
        checkout.enterLastName(lastName);
        checkout.enterPostalCode(postalCode);
        checkout.clickOnContinue();
    }

    @Then("user should get a error message on checkout page for a empty last name field")
    public void userShouldGetAErrorMessageOnCheckoutPageForAEmptyLastNameField() {
        errorMessage.toValidateLastNameRequiredErrorMessage();
    }
}
