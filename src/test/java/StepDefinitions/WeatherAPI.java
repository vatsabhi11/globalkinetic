package StepDefinitions;

import com.fasterxml.jackson.core.JsonProcessingException;
import common.WeatherStation;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import utils.Utility;

public class WeatherAPI {

    Utility utils = new Utility();
    Response response = null;
    private static final Integer STATUSCODE_CREATED = 201;
    private static final Integer STATUSCODE_DELETED = 204;
    private static Logger Log = LogManager.getLogger(WeatherAPI.class.getName());

    @Given("User is registering a weather station with {string}, {string}, {double}, {double}, {int}")
    public void registerToWeatherStation(String externalID, String name, Double longitude, Double latitude, Integer altitude) {
        try {
            utils.setBaseURIForWeatherCalls();
            WeatherStation weatherStationRegister = new WeatherStation(externalID, name, longitude, latitude, altitude);
            utils.settingUpAPIKey();
            utils.setHeaderAsContentType();
            utils.setJsonBodyInRequest(utils.pojoConvertorToPayload(weatherStationRegister));
            response = utils.weatherStationRegister(utils.Property("weatherStation_endpoint"));
            if (response.statusCode() == STATUSCODE_CREATED) {
                utils.storingValueInProperties("ID", utils.getJsonText(response, "ID"));
                Log.info("Passed: Weather Station is register successfully with ID: " + response.getBody().asString());
            } else
                Log.info("Passed:  User not able to register the weather station due to " + utils.getJsonText(response, "message"));
        } catch (JsonProcessingException e) {
            Log.error("Fail: to register the weather station");
        }

    }

    @Then("user should get a statuscode as {int}")
    public void user_should_get_a_statuscode_as(Integer statusCode) {

        utils.validatingStatusCode(response, statusCode);
    }

    @Given("that newly register weather station should be available in the Weather Map API")
    public void thatNewlyRegisterWeatherStationShouldBeAvailableInTheWeatherMapAPI() {
        utils.setBaseURIForWeatherCalls();
        utils.settingUpAPIKey();
        utils.setHeaderAsContentType();
        response = utils.apiGETCall(utils.Property("weatherStation_endpoint") + "/" + utils.Property("ID"));
        Log.info("Passed: User is able to fetch the newly created weather station" + response.getBody().asString());
    }

    @Given("that user should be able to update the weather station with {string}, {string}, {double}, {double}, {int}")
    public void updateWeatherStation(String externalID, String name, Double longitude, Double latitude, Integer altitude) {
        try {
            WeatherStation weatherStationUpdate = new WeatherStation(externalID, name, longitude, latitude, altitude);
            utils.setBaseURIForWeatherCalls();
            utils.settingUpAPIKey();
            utils.setHeaderAsContentType();
            utils.setJsonBodyInRequest(utils.pojoConvertorToPayload(weatherStationUpdate));
            response = utils.weatherStationUpdate(utils.Property("weatherStation_endpoint") + "/" + utils.Property("ID"));
            utils.storingValueInProperties("ID", utils.getJsonText(response, "id"));
            Log.info("Passed: weather station is updated successfully: " + response.getBody().asString());
        } catch (JsonProcessingException e) {
            Log.error("Fail: not able to update the weather station");
        }
    }

    @Given("that user should be able to delete the newly register weather station")
    public void thatUserShouldBeAbleToDeleteTheNewlyRegisterWeatherStation() {
        try {
            utils.setBaseURIForWeatherCalls();
            utils.settingUpAPIKey();
            response = utils.toDeleteWeatherStation(utils.Property("weatherStation_endpoint") + "/" + utils.Property("ID"));
            if (response.getStatusCode() == STATUSCODE_DELETED)
                Log.info("Passed: weather station deleted successfully");
            else
                Log.error("Fail: to delete the weather station");
        } catch (Exception e) {
            Log.error("Fail: to delete the weather station");
        }
    }

    @When("user trying to search the deleted weather station")
    public void userTryingToSearchTheDeletedWeatherStation() {

        utils.setBaseURIForWeatherCalls();
        utils.settingUpAPIKey();
        utils.setHeaderAsContentType();
        response = utils.apiGETCall(utils.Property("weatherStation_endpoint") + "/" + utils.Property("ID"));
        Log.info("Passed: User is able to fetch the newly created weather station");
    }

    @And("{string} message in the response")
    public void messageInTheResponse(String message) {
        utils.validatingMessage(response, message);
        Log.info("Passed: User is getting " + message);
    }

    @Given("that user trying to delete a non exist station")
    public void thatUserTryingToDeleteANonExistStation() {

        try {
            utils.setBaseURIForWeatherCalls();
            utils.settingUpAPIKey();
            response = utils.toDeleteWeatherStation(utils.Property("weatherStation_endpoint") + "/" + utils.Property("DeletedID"));
            if (response.getStatusCode() == 404)
                Log.info("Passed: user unable to delete the weather station");
            else
                Log.error("Fail: to validate the deletion");
        } catch (Exception e) {
            Log.error("Fail: to validate the deletion of weather station");
        }
    }


    @And("User should get a content type as {string}")
    public void userShouldGetAContentTypeAs(String contentType) {
        utils.validatingContentType(response, contentType);
    }

    @Then("User is validating the weather station register response schema")
    public void user_is_validating_the_schema_of_the_response() {
        utils.schemaValidator(response, "schemas/WeatherStation.json");
    }

    @Then("User is validating the weather station info response schema")
    public void userIsValidatingTheWeatherStationInfoResponseSchema() {
        utils.schemaValidator(response, "schemas/WeatherStationInfo.json");
    }
}
