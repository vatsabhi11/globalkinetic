package WebdriverWrapper;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import utils.Utility;

public class BaseTest {

    public WebDriver driver;
    Utility utility = new Utility();


    public WebDriver baseDriver()
    {
        String browser = utility.Property("Browser");

        if(browser.equalsIgnoreCase("Chrome")) {
            String chrome_path = System.getProperty("user.dir") + "\\drivers\\chromedriver.exe";
            System.setProperty("webdriver.chrome.driver", chrome_path);
            driver = new ChromeDriver();
            driver.manage().window().maximize();
        }

        else{

            String path = System.getProperty("user.dir") + "\\drivers\\geckodriver.exe";
            System.setProperty("webdriver.gecko.driver", path);
            FirefoxOptions options = new FirefoxOptions();
            options.setCapability("marionette",true);
            driver = new FirefoxDriver();
            driver.manage().window().maximize();

        }

        return driver;
    }





}
