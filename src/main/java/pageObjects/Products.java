package pageObjects;

import common.Product;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.Utility;

import static common.Product.*;
import static org.assertj.core.api.Assertions.assertThat;


public class Products {

    Utility utility = new Utility();
    private static Logger Log = LogManager.getLogger(Products.class.getName());
    private static final String PRODUCT_TITLE = "PRODUCTS";

    public Products(WebDriver webDriver) {
        PageFactory.initElements(webDriver, this);
    }

    @FindBy(id = "add-to-cart-sauce-labs-backpack")
    private WebElement backPack;

    @FindBy(id = "add-to-cart-sauce-labs-bike-light")
    private WebElement bikeLight;

    @FindBy(id = "add-to-cart-sauce-labs-bolt-t-shirt")
    private WebElement boltTshirt;

    @FindBy(id = "add-to-cart-sauce-labs-fleece-jacket")
    private WebElement fleeceJacket;

    @FindBy(id = "add-to-cart-sauce-labs-onesie")
    private WebElement oneSie;

    @FindBy(id = "add-to-cart-test.allthethings()-t-shirt-(red)")
    private WebElement redTshirt;

    @FindBy(id = "shopping_cart_container")
    private WebElement cart;


    @FindBy(xpath = "//span[text()='Products']")
    private WebElement title_Product;

    private static final String productPage = "//span[text()='Products']";

    public void waitUntilProductPageLoaded(WebDriverWait wait) {

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(productPage)));


    }

    public void toVerifyProductPageTitle() {

        try {
            assertThat(utility.getText(title_Product)).isEqualTo(PRODUCT_TITLE);
            Log.info("Passed: title has been verified as " + PRODUCT_TITLE);
        } catch (Exception e) {
            Log.error("Fail: page does not contains " + PRODUCT_TITLE);
        }

    }


    public void clickOnCart() {
        try {
            cart.click();
            Log.info("Passed: user clicked on the cart icon");
        } catch (Exception e) {
            Log.error("Fail: user is not able to click on the cart icon");
        }
    }


    public void userIsAddingProduct(Product product) {
        try {
            switch (product) {
                case BackPack:
                    backPack.click();
                    Log.info("Passed: user added " + BackPack);
                    break;
                case OneSie:
                    oneSie.click();
                    Log.info("Passed: user added " + BackPack);
                    break;
                case BikeLight:
                    bikeLight.click();
                    Log.info("Passed: user added " + BikeLight);
                    break;
                case RedTshirt:
                    redTshirt.click();
                    Log.info("Passed: user added " + RedTshirt);
                    break;
                case BoltTshirt:
                    boltTshirt.click();
                    Log.info("Passed: user added " + BoltTshirt);
                    break;
                case FleeceJacket:
                    fleeceJacket.click();
                    Log.info("Passed: user added " + FleeceJacket);
                    break;
            }
        } catch (Exception e) {
            Log.error("Fail: user not able to add the product");
        }

    }

}
