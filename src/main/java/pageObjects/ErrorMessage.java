package pageObjects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utils.Utility;

import static org.assertj.core.api.Assertions.assertThat;

public class ErrorMessage {

    private static Logger Log = LogManager.getLogger(ErrorMessage.class.getName());
    private static final String LOCKEDOUT_USER_MESSAGE = "Epic sadface: Sorry, this user has been locked out.";
    private static final String INCORRECT_USERNAME_PASSWORD_MESSAGE = "Epic sadface: Username and password do not match any user in this service";
    private static final String LASTNAME_REQUIRED_MESSAGE = "Error: Last Name is required";
    Utility utility = new Utility();

    public ErrorMessage(WebDriver webDriver) {
        PageFactory.initElements(webDriver, this);
    }

    @FindBy(xpath = "//h3[@data-test='error']")
    private WebElement userLoginError_Message;


    public void toValidateLockoutUserMessage() {
        try {
            assertThat(utility.getText(userLoginError_Message)).isEqualTo(LOCKEDOUT_USER_MESSAGE);
            Log.info("Passed: user is getting expected error message for locked out user" + LOCKEDOUT_USER_MESSAGE);
        } catch (Exception e) {
            Log.error("Fail: user is not getting expected error message for a locked out user");
        }
    }

    public void toValidateIncorrectUserNamePasswordMessage() {
        try {
            assertThat(utility.getText(userLoginError_Message)).isEqualTo(INCORRECT_USERNAME_PASSWORD_MESSAGE);
            Log.info("Passed: user is getting expected error message for incorrect username/password" + INCORRECT_USERNAME_PASSWORD_MESSAGE);
        } catch (Exception e) {
            Log.error("Fail: user is not getting expected error message for incorrect username/password");
        }
    }

    public void toValidateLastNameRequiredErrorMessage(){
        try {

            assertThat(utility.getText(userLoginError_Message)).isEqualTo(LASTNAME_REQUIRED_MESSAGE);
            Log.info("Passed: user is not able to enter the required checkout details due to problematic user and getting " + LASTNAME_REQUIRED_MESSAGE);
        } catch (Exception e) {
            Log.error("Fail: user is not getting expected error message for incorrect username/password");
        }
    }
}
