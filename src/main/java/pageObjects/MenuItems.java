package pageObjects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MenuItems {

    private static Logger Log = LogManager.getLogger(MenuItems.class.getName());

    public MenuItems(WebDriver webDriver) {
        PageFactory.initElements(webDriver, this);
    }

    @FindBy(id = "react-burger-menu-btn")
    private WebElement menuBox;

    @FindBy(id = "inventory_sidebar_link")
    private WebElement menu_ALLITEMS;

    @FindBy(id = "about_sidebar_link")
    private WebElement menu_ABOUT;

    @FindBy(id = "logout_sidebar_link")
    private WebElement menu_LOGOUT;

    @FindBy(id = "reset_sidebar_link")
    private WebElement menu_RESET;


    public void clickOnMenuItem() {
        try {
            menuBox.click();
            Log.info("Passed: user clicked on Menu Box");
        } catch (Exception e) {
            Log.error("Fail: user is not able to click on Menu Box");
        }
    }

    public void selectAbout() {
        try {
            menu_ABOUT.click();
            Log.info("Passed: user clicked on About");
        } catch (Exception e) {
            Log.error("Fail: user is not able to click on About");
        }

    }

    public void clickOnLogOut() {
        try {
            menu_LOGOUT.click();
            Log.info("Passed: user clicked on LogOut");
        } catch (Exception e) {
            Log.error("Fail: user is not able to click on LogOut");
        }
    }

    public void clickOnReset() {
        try {
            menu_RESET.click();
            Log.info("Passed: user clicked on Reset");
        } catch (Exception e) {
            Log.error("Fail: user is not able to click on Reset");
        }
    }
    public void clickOnAllItems() {
        try {
            menu_ALLITEMS.click();
            Log.info("Passed: user clicked on All Items");
        } catch (Exception e) {
            Log.error("Fail: user is not able to click on All Items");
        }
    }

}
