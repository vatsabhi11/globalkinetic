package pageObjects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utils.Utility;

import static org.assertj.core.api.Assertions.assertThat;

public class CheckoutComplete {

    Utility utility = new Utility();
    private static Logger Log = LogManager.getLogger(CheckOut.class.getName());
    private static final String THANKYOU_MESSAGE = "THANK YOU FOR YOUR ORDER";
    private static final String CHECKOUT_COMPLETE_TITLE = "CHECKOUT: COMPLETE!";

    public CheckoutComplete(WebDriver webDriver) {
        PageFactory.initElements(webDriver, this);
    }

    @FindBy(xpath = "//h2[text()='THANK YOU FOR YOUR ORDER']")
    private WebElement orderConfirmationMessage;

    @FindBy(xpath = "//span[@class='title']")
    private WebElement checkoutCompleteTitle;


    public void validatingOrderConfirmationMessage() {
        try {
            assertThat(utility.getText(orderConfirmationMessage)).isEqualTo(THANKYOU_MESSAGE);
            Log.info("Passed: " + THANKYOU_MESSAGE + " message has been displayed after successful order ");
        } catch (Exception e) {
            Log.error("Fail: " + THANKYOU_MESSAGE + " message has not been displayed after successful order");
        }
    }

    public void toVerifyCheckoutCompleteTitle() {
        try {
            assertThat(utility.getText(checkoutCompleteTitle)).isEqualTo(CHECKOUT_COMPLETE_TITLE);
            Log.info("Passed: title has been verified as " + CHECKOUT_COMPLETE_TITLE);
        } catch (Exception e) {
            Log.error("Fail: page does not contains " + CHECKOUT_COMPLETE_TITLE);
        }
    }
}
