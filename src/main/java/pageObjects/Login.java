package pageObjects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utils.Utility;

public class Login {

    Utility utility = new Utility();
    private static Logger Log = LogManager.getLogger(Login.class.getName());

    public Login(WebDriver webDriver) {
        PageFactory.initElements(webDriver, this);
    }

    @FindBy(id = "user-name")
    private WebElement userName;

    @FindBy(id = "password")
    private WebElement password;

    @FindBy(id = "login-button")
    private WebElement loginBtn;

    private void setUserName(String user) {
        try {
            userName.sendKeys(user);
            Log.info("Passed:" + user + " has been entered");
        } catch (Exception e) {
            Log.error("Fail: not able to enter the username");
        }
    }

    private void setPassword(String pwd) {
        try {
            password.sendKeys(pwd);
            Log.info("Passed: password has been entered");
        } catch (Exception e) {
            Log.error("Fail: user is not able to enter the password ");
        }
    }

    private void clickOnLogin() {
        try {
            loginBtn.click();
            Log.info("Passed: user clicked on the login button");
        } catch (Exception e) {
            Log.error("Fail: user is not able to click on login button");
        }
    }

    public void loginToSwagLabs(String user, String pwd) {

        setUserName(user);
        setPassword(pwd);
        clickOnLogin();
    }

    public void navigateToSwagLabs(WebDriver driver) {
        try {
            driver.get(utility.Property("SwagLabsURL"));
            Log.info("Passed: User is successfully naviated to " + driver.getTitle());
        } catch (Exception e) {
            Log.error("Fail: User is not able to hit the URL" + driver.getCurrentUrl());
        }


    }

}
