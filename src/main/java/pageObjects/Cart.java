package pageObjects;

import common.Product;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utils.Utility;

import static common.Product.*;
import static org.assertj.core.api.Assertions.assertThat;

public class Cart {

    Utility utility = new Utility();
    private static Logger Log = LogManager.getLogger(Cart.class.getName());
    private static final String CART_TITLE = "YOUR CART";

    public Cart(WebDriver webDriver) {
        PageFactory.initElements(webDriver, this);
    }


    @FindBy(xpath = "//span[text()='Your Cart']")
    private WebElement cartTitle;

    @FindBy(xpath = "//div[text()='Sauce Labs Backpack']")
    private WebElement cartBackPack;

    @FindBy(xpath = "//div[text()='Sauce Labs Bike Light']")
    private WebElement cartBikeLight;

    @FindBy(xpath = "//div[text()='Sauce Labs Onesie']")
    private WebElement cartOneSie;

    @FindBy(xpath = "//div[text()='Test.allTheThings() T-Shirt (Red)']")
    private WebElement cartRedTshirt;

    @FindBy(xpath = "//div[text()='Sauce Labs Fleece Jacket']")
    private WebElement cartFleeceJacket;

    @FindBy(xpath = "//div[text()='Sauce Labs Bolt T-Shirt']")
    private WebElement cartBoltTshirt;

    @FindBy(id = "checkout")
    private WebElement checkout;


    public void toVerifyCartPageTitle() {

        try {
            assertThat(utility.getText(cartTitle)).isEqualTo(CART_TITLE);
            Log.info("Passed: title has been verified as " + CART_TITLE);
        } catch (Exception e) {
            Log.error("Fail: page does not contains " + CART_TITLE);
        }

    }

    public void isClickonCheckout() {
        try {
            checkout.click();
            Log.info("Passed: user clicked on Checkout");
        } catch (Exception e) {
            Log.error("Fail: user is not able to click on Checkout");
        }
    }


    public void toVerifyProductInCart(Product product) {
        try {
            switch (product) {
                case BackPack:
                    assertThat(utility.getText(cartBackPack)).isEqualTo(utility.Property("BackPack"));
                    Log.info("Passed: " + BackPack + " is available in the Cart");
                    break;
                case OneSie:
                    assertThat(utility.getText(cartOneSie)).isEqualTo(utility.Property("OneSie"));
                    Log.info("Passed: " + OneSie + " is available in the Cart");
                    break;
                case BikeLight:
                    assertThat(utility.getText(cartBikeLight)).isEqualTo(utility.Property("BikeLight"));
                    Log.info("Passed: " + BikeLight + " is available in the Cart");
                    break;
                case RedTshirt:
                    assertThat(utility.getText(cartRedTshirt)).isEqualTo(utility.Property("RedTshirt"));
                    Log.info("Passed: " + RedTshirt + " is available in the Cart");
                    break;
                case BoltTshirt:
                    assertThat(utility.getText(cartBoltTshirt)).isEqualTo(utility.Property("BoltTshirt"));
                    Log.info("Passed: " + BoltTshirt + " is available in the Cart");
                    break;
                case FleeceJacket:
                    assertThat(utility.getText(cartFleeceJacket)).isEqualTo(utility.Property("FleeceJacket"));
                    Log.info("Passed: " + FleeceJacket + " is available in the Cart");
                    break;
            }
        } catch (Exception e) {
            Log.error("Fail: " + product + " is not available in the cart");
        }

    }

}
