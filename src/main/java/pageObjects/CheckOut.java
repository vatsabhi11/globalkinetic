package pageObjects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utils.Utility;

public class CheckOut {


    Utility utility = new Utility();
    private static Logger Log = LogManager.getLogger(CheckOut.class.getName());

    public CheckOut(WebDriver webDriver) {
        PageFactory.initElements(webDriver, this);
    }

    @FindBy(id = "first-name")
    private WebElement firstName;

    @FindBy(id = "last-name")
    private WebElement lastName;

    @FindBy(id = "postal-code")
    private WebElement postalCode;

    @FindBy(id = "continue")
    private WebElement continueBtn;

    @FindBy(id = "finish")
    private WebElement finish;


    public void enterFirstName(String fname) {
        try {
            firstName.sendKeys(fname);
            Log.info("Passed: user entered firstname as " + fname + " in the checkout details");
        } catch (Exception e) {
            Log.error("Fail: user is not able to enter firstname in the checkout details");
        }
    }

    public void enterLastName(String lName) {
        try {
            lastName.sendKeys(lName);
            Log.info("Passed: user entered lastname as " + lName + " in the checkout details");
        } catch (Exception e) {
            Log.error("Fail: user is not able to enter lastname in the checkout details");
        }
    }

    public void enterPostalCode(String code) {
        try {
            postalCode.sendKeys(code);
            Log.info("Passed: user entered postalcode as " + code + "in the checkout details");
        } catch (Exception e) {
            Log.error("Fail: user is not able to enter postalcode in the checkout details");
        }
    }

    public void clickOnContinue() {
        try {
            continueBtn.click();
            Log.info("Passed: user clicked on Continue button");
        } catch (Exception e) {
            Log.error("Fail: user is not able to click on Continue button");
        }
    }

    public void clickOnFinish() {
        try {
            finish.click();
            Log.info("Passed: user clicked on Finish button");
        } catch (Exception e) {
            Log.error("Fail: user is not able to click on Finish button");
        }
    }

}
