# README #

This README would normally document whatever steps are necessary to get your application up and running.


## Set up

clone the repository into Intellij IDEA using version control

## Excecution
Use TestRunner to execute the test and to generate the reports

### What is this repository for? ###
Global Kinetic SDET Assessment

1. Swag Labs UI Automation
   Test scenarios has been mentioned in the feature file named SwagLabs.feature,
   implementation for the same in available in WeatherAPI: src/test/java/StepDefinitions/SwagLabs.java
   path for feature file : src/test/resources/features/SwagLabs.feature
   Browser independent- It can work on any browser- currently working functionality has been coded.
   How to change browser? - Go to Environment.properties files -> Browser=chrome and by default it will work on firefox.
   Browser Compatible: 
            Chrome: Version 95.0.4638.69 (Official Build) (64-bit)
            Firefox: 94.0.1 (64-bit)
   
2 . Weather Map API Automation
   Test scenarios has been mentioned in the feature file named WeatherMap.feature,
   implementation for the same in available in WeatherAPI: src/test/java/StepDefinitions/WeatherAPI.java
   path for feature file : src/test/resources/features/WeatherMap.feature


## Validations performed

1. Status code validation for each call
2. headers validation for each call
3. Message validation for response wherever requried
4. Applicable assertions has been placed using AssertJ.
5. Schema Validation has been performed on API's call.

## Scnearios Coverage
There are two contexts which has been defined in the project as below:
1. Success context - Scenario is created to perform the overall happy path flow which has been asked to automate as per the assessment.
2. Error context - Scenario to perform negative testing in the UI and API.


## Coding Standards

1.Reusable functions: Reusable functions has been defined in the "utils" package under "Utility.java" class. To decrease the
complexity of the code.
2.Pojo class has been created to set the payload for the post/put calls.
3.Clean code: to make sure the code should look clean and will be easy to maintain , all the functions has been
named meaningfully to avoid unnecessary comments within the code.

## Logging

log4j2 has been used to perform logging in the framework to showcase the flow the tests which are running.

## Report
report for test execution has been present in report directory under the name GlobalKinetic.html.

## Configuration
This directory has been created to declare variables globally

## Tools
UI Automation : Selenium
API Automation : Rest Assured
Reporting: Extent Reports
Build Tool: Maven
BDD: Cucumber
IDE: Intellij IDEA
Testing Framework: Junit
Language : Java
Logging  : log4j2


